#ifndef _MENU_H
#define _MENU_H


#define MAKE_MENU(Name, Next, Child, Select,Enter, variable_display) \
	const menuItem Next;\
	const menuItem  Child;\
	const menuItem Name = {(void*)&Next, (void*)&Child, (int) Select, (int)  Enter, variable_display }

#define NULL_ENTRY Null_Menu
#define NULL_FUNC  (int)0
#define NULL_TEXT  0x00
#define NEXT       ((menuItem*)(selectedMenuItem->Next))
#define CHILD      ((menuItem*)(selectedMenuItem->Child))
#define SELECTFUNC   ((FuncPtr)(selectedMenuItem->SelectFunc))
#define ENTERFUNC   ((FuncPtr)(selectedMenuItem->EnterFunc))   

 typedef void (*FuncPtr)(void);

 typedef struct
{
   void      *Next;
   void      *Child;
   int     SelectFunc;   // ��������� ��� ����������� � ����� ����
   int     EnterFunc;   // ��������� ������, ���������� ��� ������� Enter � ����
   int   variable_display; /*���������� ���������*/
} menuItem;




#endif